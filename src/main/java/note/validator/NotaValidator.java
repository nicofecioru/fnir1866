package note.validator;

import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;

public class NotaValidator {
    public static boolean validareNota(Nota nota) throws ClasaException {
        // TODO Auto-generated method stub
        if(nota.getMaterie().length() < 1 || nota.getMaterie().length() > 20)
            throw new ClasaException(Constants.invalidMateria);
        if(nota.getNrmatricol() < Constants.minNrmatricol || nota.getNrmatricol() > Constants.maxNrmatricol)
            throw new ClasaException(Constants.invalidNrmatricol);
        if(nota.getNota() < Constants.minNota || nota.getNota() > Constants.maxNota)
            throw new ClasaException(Constants.invalidNota);
        if(nota.getNota() != (int)nota.getNota())
            throw new ClasaException(Constants.invalidNota);
        if(nota.getNrmatricol() != (int)nota.getNrmatricol())
            throw new ClasaException(Constants.invalidNrmatricol);
        return true;
    }
}
