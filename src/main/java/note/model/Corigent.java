package note.model;

public class Corigent extends Elev{
	private int nrMaterii;

	public Corigent(int nrMatricol, String nume, int nrMaterii) {
		super(nrMatricol, nume);
		this.nrMaterii = nrMaterii;
	}

	/**
	 * @return the nrMaterii
	 */
	public int getNrMaterii() {
		return nrMaterii;
	}
	/**
	 * @param nrMaterii the nrMaterii to set
	 */
	public void setNrMaterii(int nrMaterii) {
		this.nrMaterii = nrMaterii;
	}

	public String toString() {
		return nume + " -> " + nrMaterii;
	}
}
