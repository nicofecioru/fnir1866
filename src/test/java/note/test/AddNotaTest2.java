package note.test;

import static org.junit.Assert.*;

import note.model.Nota;

import note.utils.ClasaException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import note.utils.Constants;

import note.controller.Controller;

public class AddNotaTest2 {

    private Controller ctrl;

    @Before
    public void init() {
        ctrl = new Controller();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws ClasaException {
        ctrl.addNota2(100, "desen", 10);
        assertEquals(1, ctrl.getNote().size());
    }
}
