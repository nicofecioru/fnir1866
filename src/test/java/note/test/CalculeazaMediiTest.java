package note.test;

import static org.junit.Assert.*;

import java.util.List;

import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

import note.utils.ClasaException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import note.utils.Constants;

import note.controller.Controller;

public class CalculeazaMediiTest {
	
private Controller ctrl;
	
	@Before
	public void init(){
		ctrl = new Controller();
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Test
	public void test1() throws ClasaException {
		Elev e1 = new Elev(1, "Elev1");
		ctrl.addElev(e1);
		Nota n1 = new Nota(1,"Materie1", 1);
		ctrl.addNota(n1);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		//ctrl.afiseazaClasa();
		List<Medie> rezultate = ctrl.calculeazaMedii();
		for(Medie m : rezultate)
			if(m.getElev().getNrmatricol() == 1)
				assertEquals(m.getMedie(),1,0.0001);
	}
	
	@Test
	public void test2() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.emptyRepository);
		ctrl.calculeazaMedii();
	}
	
	@Test
	public void test3() throws ClasaException {
		Elev e1 = new Elev(1, "Elev1");
		ctrl.addElev(e1);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Medie> rezultate = ctrl.calculeazaMedii();
		for(Medie m : rezultate)
			if(m.getElev().getNrmatricol() == 1)
				assertEquals(m.getMedie(),0,0.0001);
	}
	
}
