package note.controller;

import note.utils.ClasaException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class ControllerTest {


    private Controller ctrl;

    @Before
    public void init() {
        ctrl = new Controller();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void addNota21() throws Exception {
        ctrl.addNota2(100, "desen", 10);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void addNota22(){
        try {
            ctrl.addNota2(100, "", 10);
            assertTrue(false);
        } catch (ClasaException e){
            assertTrue(true);
        }
    }

    @Test
    public void addNota23() {
        try {
            ctrl.addNota2(100, "desen", 11);
            assertTrue(false);
        } catch (Exception e){

        }
    }

    @Test
    public void addNota24() throws Exception {
        ctrl.addNota2(100, "m", 10);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void addNota25() throws Exception {
        ctrl.addNota2(100, "m1234567891234567890", 10);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void addNota26() throws Exception {
        ctrl.addNota2(100, "m123456789123456789", 10);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void addNota27() {
        try {
            ctrl.addNota2(100, "m12345678912345678901", 10);
            assertTrue(false);
        } catch (Exception e){

        }
    }

    @Test
    public void addNota28() {
        try {
            ctrl.addNota2(100, "m", 0);
            assertTrue(false);
        } catch (Exception e){

        }
    }

    @Test
    public void addNota29() throws Exception {
        ctrl.addNota2(100, "desen", 2);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void addNota210() throws Exception {
        ctrl.addNota2(100, "desen", 9);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void addNota211() throws Exception {
        ctrl.addNota2(100, "desen", 1);
        assertEquals(1, ctrl.getNote().size());
    }
}